const fdk = require('@fnproject/fdk');
const axios = require('axios')
const _ = require('lodash')

const isbnRegex = /^(?:ISBN(?:-1[03])?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)(?:97[89][- ]?)?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$/;

fdk.handle(async function (input, ctx) {
  // set headers
  let hctx = ctx.httpGateway
  hctx.setResponseHeader("Access-Control-Allow-Origin", "*")
  hctx.setResponseHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")


  // check for given inputs
  if (!input.isbn) {
    return { 'error': "No ISBN specified" }
  }

  //match isbn regex
  if (!input.isbn.match(isbnRegex)) {
    return { 'error': "The given ISBN is not valid" }
  }
  try {
    //try to get cover from openlibrary
    let coverRequest = await axios.get(`http://covers.openlibrary.org/b/isbn/${input.isbn}-L.jpg?default=false`, { responseType: 'arraybuffer' })
    const imageBase64 = Buffer.from(coverRequest.data, 'binary').toString('base64')
    return { 'status': 'success', 'image': imageBase64 }
  }
  catch (error) {
    try {
      //try to get cover from google books api, if openlibrary failed
      console.log('try google')
      let coverUrlRequest = await axios.get(`https://www.googleapis.com/books/v1/volumes?q=isbn:${input.isbn}`)
      coverUrl = coverUrlRequest.data.items[0].volumeInfo.imageLinks.thumbnail
      let coverRequest = await axios.get(coverUrl, { responseType: 'arraybuffer' })
      const imageBase64 = Buffer.from(coverRequest.data, 'binary').toString('base64')
      return { 'status': 'success', 'image': imageBase64 }
    }
    catch (error) {
      console.log(error)
      return { 'status': 'failed' }
    }
  }
})