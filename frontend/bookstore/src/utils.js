function Book({isbn = '', title = '', author = '', genre = '', pages = '', cover = '', summary = ''}) {
    return { isbn, title, author, genre, pages, cover, summary }
}
export default Book;