import React, { useState } from 'react';
import Card from 'react-bootstrap/Card'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import './CreateNewBook.css'
import Book from '../utils'
import axios from 'axios'
import API_URI from '../constants';
import BookScanner from '../BookScanner/BookScanner';
import CameraAltIcon from '@material-ui/icons/CameraAlt';

function sendBook(input, reloadBooks) {
  axios.post(API_URI + 'addbook', input).then(response => {
    reloadBooks()
    console.log('Success', response)
  })
}

function fetchBookFromAPI(input, setInput, setFetchFailed) {
  setFetchFailed(false)
  axios.post(API_URI + 'addbookbyisbn', { 'isbn': input }).then(response => {
    if (!response.data.book) {
      console.warn(response.data.error)
      setFetchFailed(true)
    } else {
      const bookToSet = Book(response.data.book)
      setInput({...bookToSet})
    }
  }).catch(error => {
    console.warn(error)
    setFetchFailed(true)
  })
}

// {"isbn":"9780596999999",
//  "title":"JavaScript: the super good parts",
//  "author":"Douglas Crockford",
//  "genre":"programming",
//  "pages":153,
//  "cover":"https://someurl",
//  "summary":"Most programming languages ..."
// }

function CreateNewBook(props) {
  const [inputValues, setInput] = useState(Book({}))
  const [fetchFailed, setFetchFailed] = useState(false)
  const [isbnScannerOpen, setISBNScannerOpen] = useState({open: false, isbn: ""})


  return (
    <Card className="text-center margin-card">
       {isbnScannerOpen.open && <BookScanner isOpen={isbnScannerOpen.open} setOpen={value => {setISBNScannerOpen(value); setInput({...inputValues, isbn: value.isbn})}} />}
      <Card.Header>create new book</Card.Header>
      <Card.Body>
        <Form>
          <Button variant="primary" block onClick={e => setISBNScannerOpen({open: true, isbn: ""})}>
            <CameraAltIcon /> Scan ISBN
            </Button>
            <hr></hr>
          <Form.Group controlId="formIsbn">
            <Form.Label>ISBN*</Form.Label>
            <InputGroup >
              <FormControl
                placeholder="Enter ISBN"
                type="text"
                value={inputValues.isbn}
                onChange={e => setInput({ ...inputValues, isbn: e.target.value })}
              />
             
              <InputGroup.Append>
                <Button
                  variant="outline-secondary"
                  onClick={() => fetchBookFromAPI(inputValues.isbn, setInput, setFetchFailed)}>
                  autofill!
                  </Button>
              </InputGroup.Append>
              
            </InputGroup>
            {fetchFailed && <Form.Text className="text-danger">
              Autofill failed, please enter data manually
              </Form.Text>}
          </Form.Group>

          <Form.Group controlId="formTitle">
            <Form.Label>Title*</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Title"
              value={inputValues.title}
              onChange={e => setInput({ ...inputValues, title: e.target.value })}
            />
          </Form.Group>
          <Form.Group controlId="formAuthor">
            <Form.Label>Author*</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Author"
              value={inputValues.author}
              onChange={e => setInput({ ...inputValues, author: e.target.value })}
            />
          </Form.Group>
          <Form.Group controlId="formGenre">
            <Form.Label>Genre*</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Genre"
              value={inputValues.genre}
              onChange={e => setInput({ ...inputValues, genre: e.target.value })}
            />
          </Form.Group>
          <Form.Group controlId="formPages">
            <Form.Label>Pages*</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter number of pages"
              value={inputValues.pages}
              onChange={e => setInput({ ...inputValues, pages: e.target.value })}
            />
          </Form.Group>
          <Form.Group controlId="formCover">
            <Form.Label>Cover*</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Cover URL"
              value={inputValues.cover}
              onChange={e => setInput({ ...inputValues, cover: e.target.value })}
            />
          </Form.Group>
          <Form.Group controlId="formSummary">
            <Form.Label>Summary*</Form.Label>
            <Form.Control
              as="textarea"
              rows="3"
              placeholder="Enter book description"
              value={inputValues.summary}
              onChange={e => setInput({ ...inputValues, summary: e.target.value })}
            />
          </Form.Group>
          <Button
            variant="primary"
            onClick={() => sendBook(inputValues, props.reloadBooks)}
          >
            Submit
          </Button>
        </Form>
      </Card.Body>
    </Card>
  );
}

export default CreateNewBook;
