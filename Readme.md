# fn project

We will use `nodeJS (Javascript)` as environment/programming language

## Requirements:

1. Install docker
2. Install a editor of your choice, populare free choices are [Atom](https://atom.io/) by GitHub or  [Visual Studio Code](https://code.visualstudio.com/) by Microsoft
3. Install fn as described in https://github.com/fnproject/fn
4. Install nodeJs as described here: https://nodejs.org/en/

## 1. Developing first fn function & app - helloApp

Open a shell and start fn by running 
```bash
fn start
# By default it will start on port 8080, if you 
# want to use another port run
# fn start -p <your port>
``` 

Create a new development folder for future fn files with 
```bash
# open a new terminal
mkdir fnproject
cd fnproject
```
and switch to the directory. To create a new function called `hello` in the subdirectory `hello` open a new shell in the previously created `fnproject` directory and run 
```bash
fn init --runtime node hello
``` 

Create a new app (a collection of functions) to access these functions. Execute the following command to create the app `helloapp`
```bash
fn create app helloapp
```

You can list all created apps with the command 
```bash
fn list apps
```

To add the function `hello` to our newly created app `helloapp`  switch to the `hello` subdirectory and execute 
```bash
fn deploy --app helloapp --local
```

The newly generated function inside the app can be run by 
```bash
fn invoke helloapp hello
``` 

Congratulations, you will see the output "Hello World"
## The fn file structure
Open the `fnproject` folder in your favorite editor. You will see a subdirectory `hello` from the newly created function `hello`. Inside the subdirectory, you can see three files, `func.js`, `func.yaml` and a `package.json`. The file `func.js` contains the newly created function. 

In this Hello world example, it returns a simple hello world by default. If a `name` parameter is passed at the beginning, it will display `Hello {$name}`

Run the following command to replace World with your name 
```bash 
echo -n '{"name": "Tim"}' | fn invoke helloapp hello
```
Now we will have a closer look at `func.yaml` and `package.json`. 

The `package.json` is present in nearly every nodeJS project. It lists all the node dependencies, a licence, author, startpoint and more details of the function. 

The `func.yaml` serves the same purpose and is not node exclusive, but necessary for fn project to work. It contains the name, a version number, the runtime environment and a execution entrypoint.

## Using the integrated API

It is also possible to execute a local function using a HTTP REST call. To get the API endpoint URL, type 
```bash
fn inspect function helloapp hello
```
into the shell. At the top, you should see a URL similar to `http://localhost:8080/invoke/01DQ4KZM7MNG8G00GZJ0000002`. Now, you could use a REST Client of your choice or curl to call the function. Make sure to use Post to the array and add the content type header `application/json`.

Example curl call: 

```bash
curl -X "POST" -H "Content-Type: application/json" http://localhost:8080/invoke/01DQ4KZM7MNG8G00GZJ0000002
```
   To set a human readable endpoint url for your application, you could use the fn trigger function. To achieve this goal, execute the following steps:

1. make sure you are in the `fnproject` directory
2. create a new funciton hellohttp with http trigger
   ```bash
   fn init --runtime node --trigger http hellohttp
   ```

2. deploy new function into existing helloapp
   ```bash
   cd hellohttp
   fn deploy --app helloapp --local
   ```
The trigger endpoint will be set to  `http://localhost:8080/t/helloapp/hellohttp`

## 2. Enhance the book app

Start by cloning the repository and start the required apps
```bash
# clone the existing bookApp repository
git clone https://gitlab.com/timhyperledger/fnproject-bookstore.git
cd fnproject-bookstore
# change directory to jsonServer
cd jsonServer

# start json server
# it will serve as a light database for our app at port 3000. The books are stored in the db.json file.
npx json-server --watch db.json

# open a new terminal in the current folder
# start the frontend to visualize the results at port 3001
cd ../frontend/bookstore
npm i
npm start
```

![App architecture](./Architecture.jpg "App architecture")
Above you can see the basic structure of the application. The functions `getAllBooks` and `addBookByISBN` are already finished, the others are yet to be implemented. To show all current books and add new books to the store, the frontend calls the fn functions to get the data. Afterwards the functions are calling the json Server to get the raw data.

As you can see, the frontend is running now, but there was an error fetching the book data. We have to deploy our functions locally to make them accessible for the frontend.
```bash
# open a new terminal and go to the cloned directory
fn create app bookApp
cd addBookByISBN
fn deploy --app bookApp --local
cd ../getAllBooks
fn deploy --app bookApp --local
fn list functions bookApp

# output
NAME           IMAGE                ID
addbookbyisbn  addbookbyisbn:0.0.12	01DRTZ9FC0NG8G00GZJ000048G
getallbooks	   getallbooks:0.0.6    01DRTZ9JFTNG8G00GZJ000048J
```
Now our functions are deployed but can not be accessed from the webapp. Therefore have to define triggers
```bash
#create trigger for function
fn create trigger bookApp getallbooks getallbookstrigger --type http --source /getallbooks
fn create trigger bookApp addbookbyisbn addbookbyisbntrigger --type http --source /addbookbyisbn

fn list triggers bookApp
# output:
FUNCTION       NAME		            ID                         TYPE  SOURCE         ENDPOINT
addbookbyisbn  ddbookbyisbn         01DR9K2P0HNG8G00GZJ0000024 http  /addbookbyisbn http://localhost:8080/t/bookApp/addbookbyisbn
getallbooks	   getallbookstrigger	01DR9E8WDMNG8G00GZJ0000005 http  /getallbooks   http://localhost:8080/t/bookApp/getallbooks
```
Try the webapp again. Now there should be books available.

## Now it's up to you to choose a task from the three TODO_* files and complete the application. Happy coding!

If you experience troubles while coding, debugging the app and accessing the logs can be useful. 
To view the logs locally, you can start the local fn server in debug mode by running
```bash
fn start --log-level=debug
```
Additionally, logs can easily be viewed in [papertrail](https://www.papertrail.com/), a cloud-hosted log management.

To set up papertrail, create a free account and create a new log destination (TCP and UDP) in the settings menu. Afterwards, a link with the format `logs<number>.papertrailapp.com:<port>` is created. To send the fn project logs to papertrail, you have to update the settings. 
```bash
fn update app bookApp --syslog-url udp://<your-papertrail-url>
```
## Cleanup

To remove the application from your computer, type `fn delete app helloapp` and `fn delete app bookApp`. Afterwards simply delete the Code4Fun Fn Meetup folder. Additionally, you could also deinstall Docker, fn project, nodeJs and the editor of your choice if you plan no further coding.